import random,string

def password(length,num=False,strength="weak"):
    """length of password, num if you want a no. and 
    strength (weak,strong,very) """
    lower = string.ascii_lowercase
    upper = string.ascii_uppercase
    letters = lower+upper
    digits = string.digits
    punct  = string.punctuation
    pwd = ""
    if strength =="weak":
        if num:
            length -=2
            for it in range(2):
                pwd += random.choice(digits)
        for iterator in range(length):
            pwd += random.choice(lower)
    

    elif strength == "strong":
        if num: 
            length -=2
            for it in range(2):
                pwd += random.choice(digits)
        
        for i in range(length):
            pwd += random.choice(letters)

    elif strength =="very":
        ran = random.randint(2,4)
        if num: 
            length -= ran
            for i in range(ran):

                 pwd += random.choice(digits)
        length -= ran
        for it in range(ran):
            pwd += random.choice(punct)
        for it in range(length):
            pwd  += random.choice(letters)

    pwd = list(pwd)
    random.shuffle(pwd)
    return "".join(pwd)



print(password(5,num=True))
print(password(10,num=True,strength="strong"))
print(password(15,num=True,strength="very"))
